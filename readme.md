# 1. Development setup

## Initial setup:

Clone the repo
Run ```sudo npm install --global gulp``` in case you don't have gulp installed
Otherwise run ```npm install``` to install the dependencies

## Server start:

Run ```gulp watch``` in order to build and start the server

## Other commands:

Run ```gulp clean``` to clean your *'build'* directory
Use ```gulp``` to build your files

# 2. Making use of pug template

## 2.1 Header Blocks

By default in the `html` are included the following blocks:

### block depth

This block contains only a variable created as a helper for defining paths to styles, scripts and meta files(favicons or og-metas).
Assuming you'll be using the default locations for these (root), you can simply define the `pagedepth` variable with `../` and it will update all items.

### block style

This block serves [Bootstrap](https://getbootstrap.com) and the style that will be outputted by your stylus syntax.

### block head-meta

Extend this block and add the page's title as value to  `pagetitle` variable in your pug files.

### block script

By default here are injected all the bootstrap scripts.

## 2.2 Setting up your menu

Set your default value for menu in `var selected` (make sure its label is found in `var menu`)
Add the **menu items you want** in `var menu` by following this ```'label-name' : '/path'```

## 2.3 Scripts

### Open link in a new tab

1. Description

This script allows you to inject `target=_blank` atribute to all links that lead to external websites. It's mostly necessary if you're making use of `:markdown-it` package.

2. Configuration

Locate `baseURL` and replace `localhost` with your domain. Don’t include *https* or *www*.

# 3 Folders and architecture

### server-files directory

Here you include all the files that need to be injected in the `build` directory, which will be the root directory.
Files such as

- sitemap.xml
- fav icons
- og-images
- scripts

### src directory

This is where all the magic happens. In the main directory you’ll see folders such as:

- components
- img
- style
- svg

But also files such as:

- index.pug—this is your homepage
- main.layout.pug—this is the template we talked in Chapter 2

So, what are these directories meaning?
#### components directory

Include in this directory other website directories and sub-pages in order to keep `src` clean.

#### img directory

Include in this directory all the website’s images but limit it to `.png`, `.jpg` and other bitmap image formats.

#### style directory

Everything included in `astyle.styl` will be compiled in `common.min.css`. Everything that’s not included in that file will be ignored.
In the `common` folder are included your branding style (navigation, colours, typography and so on).
If you need to create more styles for different sub-pages, I recommend you to create a folder for that specific section of the website and include it in `astyle.styl`.

#### svg directory

Include here any svg that you need in your website. On running the gulp task, these will be minified and moved in your `build/svg` directory.

# 4. Using the template

Create your page's pug file and add use the following example code-block to extend and personalise your page by giving the necessary values to the variables mentioned (re-read chapter 2 for details)

```pug
extends ../../main.layout.pug
append depth
  -var pagedepth = "../"
prepend head-meta
  - var pagetitle = "Product designer with over ten years experience in creating  intuitive and beautiful software"
block nav-link
  -var selected = "Work"

block content
  p Content goes here
```