/*jshint esversion: 6 */
const gulp = require('gulp');
const sequence = require('gulp-sequence');
const pug = require('gulp-pug');
const stylus = require('gulp-stylus');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const svgmin = require('gulp-svgmin');
const all = require('gulp-all');
const clean = require('gulp-clean');
const plumber = require('gulp-plumber');
const pump = require('pump');
const notify = require('gulp-notify');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();

gulp.task('compile', () => {
  return gulp.src([
    'src/components/**/*.pug',
    'src/*.pug',
    '!src/**/*.layout.pug'
  ])
    .pipe(plumber({ errorHandler: function(err) {
      notify.onError({
          title: "Gulp error in " + err.plugin,
          message:  err.toString()
      })(err);
    }}))
    .pipe(pug())
    .pipe(gulp.dest('build/'));
});

gulp.task('css-compile', () => {
  return all(
    gulp.src('src/style/astyle.styl')
      .pipe(plumber({ errorHandler: function(err) {
        notify.onError({
            title: "Gulp error in " + err.plugin,
            message:  err.toString()
        })(err);
      }}))
      .pipe(stylus())
      .pipe(concatCss("common.min.css"))
      .pipe(cleanCSS())
      .pipe(gulp.dest('build/')),
    gulp.src('src/components/**/*.styl')
      .pipe(plumber({ errorHandler: function(err) {
        notify.onError({
            title: "Gulp error in " + err.plugin,
            message:  err.toString()
        })(err);
      }}))
      .pipe(stylus())
      .pipe(cleanCSS())
      .pipe(gulp.dest('build/'))
  );
});

gulp.task('svg', () => {
  return gulp.src('src/svg/**/*.svg')
    .pipe(plumber({ errorHandler: function(err) {
      notify.onError({
          title: "Gulp error in " + err.plugin,
          message:  err.toString()
      })(err);
    }}))
    .pipe(svgmin())
    .pipe(gulp.dest('build/svg'))
    .pipe(browserSync.reload({ stream:true }));
});

gulp.task('clean', () => {
  return gulp.src(['build/*', 'build/.*'], {read: false})
    .pipe(clean());
});

// Multi copy paths
gulp.task('copy', () => {
  return all (
    gulp.src(['server-files/*', 'server-files/.*'])
      .pipe(gulp.dest('build/')),
    gulp.src('./src/js/*.min.js')
      .pipe(gulp.dest('build/js/')),
    gulp.src(['src/img/**/*', '/src/img/.*'])
      .pipe(gulp.dest('build/img/')),
    gulp.src('src/components/thoughts/assets/**/*')
      .pipe(gulp.dest('build/thoughts/assets/'))
  );
});

gulp.task('js:minify', function (cb) {
  pump([
      gulp.src([
        './src/js/*.js',
        '!./src/js/*.min.js'
    ]),
      babel({
        presets: ['es2015']
      }),
      uglify(),
      concat('vendor.min.js'),
      gulp.dest('./build/js/')
    ],
    cb
  );
});

gulp.task('watch', ['default'], () => {
  browserSync.init({
    server: {
      baseDir: './build',
      routes: {
        "/node_modules": "node_modules"
      }
    }
  });

  gulp.watch('src/**/*.pug', ['compile']).on('change', browserSync.reload);
  gulp.watch('src/svg/**/*.svg', ['svg']).on('change', browserSync.reload);
  gulp.watch('src/img/**/*.+(png|jpg|jpeg)', ['copy']).on('change', browserSync.reload);
  gulp.watch('server-files/*', ['copy']).on('change', browserSync.reload);
  gulp.watch('src/js/*.js', ['js:minify']).on('change', browserSync.reload);
  gulp.watch(['src/style/common/**/*.styl', 'src/components/**/*.styl', 'src/style/astyle.styl'], ['css-compile']).on('change', browserSync.reload);
});

gulp.task('build', function(callback) {
  sequence(
    'clean',
    'svg',
    'css-compile',
    'copy',
    'js:minify',
    'compile'
  )(callback);
});

gulp.task('default', ['build']);
