document.addEventListener('DOMContentLoaded', function(event) {
  let baseURL = 'localhost';

  document.getElementById('year').innerHTML = `Copyright &copy;  ${new Date().getFullYear()}  <a href="#">Company</a>. All rights reserved.`

  let anchorTags = document.querySelectorAll('a');

  anchorTags.forEach(function(tag) {
    if (!(tag.href.includes(baseURL))) {
      tag.setAttribute('target', '_blank');
      tag.setAttribute('rel', 'noreferrer');
    }
  });
});

(function(){
  var burger = document.querySelector('.burger-container'),
      header = document.querySelector('.header');

  burger.onclick = function() {
      header.classList.toggle('menu-opened');
  }
}());